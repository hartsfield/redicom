package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"golang.org/x/net/html"
)

func main() {
	groups := []string{
		"cluster",
		"connection",
		"geo",
		"hash",
		"hyperloglog",
		"generic",
		"list",
		"pubsub",
		"scripting",
		"server",
		"set",
		"sorted_set",
		"string",
		"transactions",
	}

	r, err := http.Get("https://redis.io/commands")
	if err != nil {
		log.Panicln(err, "Looks like the page wouldn't load, did you use the correct address?")
	}

	defer func() {
		err := r.Body.Close()
		if err != nil {
			fmt.Println("Error closing response body, this should not effect anything: ", err)
		}
	}()

	n, _ := html.Parse(r.Body)
	for _, v := range groups {
		fmt.Println("============================================================")
		fmt.Println("  ", strings.ToUpper(v))
		fmt.Println("============================================================")

		docs := findCommands(n, v)
		for _, v := range docs {
			fmt.Println(v.Command + " - " + v.Description)
		}
	}
}

type commandDoc struct {
	Group       string
	Command     string
	Description string
	Article     string
}

// PARSE FUNCTIONS

func findCommands(n *html.Node, group string) []commandDoc {
	// Typical parse function from the docs
	var docs []commandDoc
	var parseFunc func(*html.Node)
	parseFunc = func(n *html.Node) {
		if n.Type == html.ElementNode && len(n.Attr) >= 1 {
			if n.Attr[0].Key == "data-group" && n.Attr[0].Val == group {
				name := findName(n)
				description := findSummary(n)
				docs = append(docs, commandDoc{Command: name, Group: group, Description: description})
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			parseFunc(c)
		}
	}
	parseFunc(n)
	return docs
}

func findName(n *html.Node) string {
	// Typical parse function from the docs
	var name string
	var parseFunc func(*html.Node)
	parseFunc = func(n *html.Node) {
		if n.Type == html.ElementNode && len(n.Attr) >= 1 {
			if n.Attr[0].Val == "command" {
				name = "         " + strings.TrimSpace(strings.Split(n.FirstChild.Data, "\n")[1])
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			parseFunc(c)
		}
	}
	parseFunc(n)
	return name
}

func findSummary(n *html.Node) string {
	// Typical parse function from the docs
	var summary string
	var parseFunc func(*html.Node)
	parseFunc = func(n *html.Node) {
		if n.Type == html.ElementNode && len(n.Attr) >= 1 {
			if n.Attr[0].Val == "summary" {
				summary = strings.TrimSpace(strings.Split(n.FirstChild.Data, "\n")[0])
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			parseFunc(c)
		}
	}
	parseFunc(n)
	return summary
}
